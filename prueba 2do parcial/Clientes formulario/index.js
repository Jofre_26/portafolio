const mysql = require('mysql');
const connection = mysql.createConnection({
host: "localhost",
user: "root",
password: "",
database: "empresa",
});
connection.connect((error)=>{
if(error) {
console.log('Error de conexion: '+error);
return;
}
console.log('Conexion exitosa!');
});
const express = require('express');
const app = express();
app.use(express.urlencoded({extended:false}));
app.use(express.json());
app.set('view engine', 'hbs');
const req = require('express/lib/request');
const res = require('express/lib/response');
const { redirect } = require('express/lib/response');
app.get("/", (req, res) => {
    res.render("formulario",{valor:false,mensaje:""});
  });
app.post("/", async (req, res) => {
  // //validacion esta en el formulario.hbs
  // const expresiones = {
  //   cedula: /^\d{10}$/,
  //   nombre: /^[a-zA-ZÀ-ÿ\s]{1,50}$/, 
  //   direccion: /^[a-zA-Z0-9À-ÿ\s]{1,60}$/, 
  //   telefono: /^\d{10}$/, 
  //   correo: /\S+@\S+.\S+/,
  // }
  // function validar() {
  //   var datosCorrectos = true;
  //   var error1 = "";
  //   var error2 = "";
  //   var error3 = "";
  //   var error4 = "";
  //   var error5 = "";
  //   if (document.getElementById("Cedula_Cliente").value == "" || !expresiones.cedula.test(document.getElementById("Cedula_Cliente").value)) {
  //       datosCorrectos = false;
  //       error1 = "\nLa cedula tiene que tener solo 10 digitos numericos"
  //   }

  //   if (document.getElementById("Nombres_Cliente").value == "" || !expresiones.nombre.test(document.getElementById("Nombres_Cliente").value)) {
  //       datosCorrectos = false;
  //       error2 = "\nLos nombres solo puede llevar letras y hasta 50 digitos"
  //   }
  //   if (document.getElementById("Direccion_Cliente").value == "" || !expresiones.direccion.test(document.getElementById("Direccion_Cliente").value)) {
  //       datosCorrectos = false;
  //       error3 = "\nLa direccion debe ser de 1 a 60 carácteres"
  //   }
  //   if (document.getElementById("Telefono_Cliente").value == "" || !expresiones.telefono.test(document.getElementById("Telefono_Cliente").value)) {
  //       datosCorrectos = false;
  //       error4 = "\nEl telefono tiene que tener solo 10 digitos numericos"
  //   }
  //   if (document.getElementById("Correo_Cliente").value == "" || !expresiones.correo.test(document.getElementById("Correo_Cliente").value)) {
  //       datosCorrectos = false;
  //       error5 = "\nEl correo no es valido"
  //   }
  //   if (!datosCorrectos) {
  //       alert('Hay errores en el formulario' + error1 + error2 + error3 + error4 + error5);
  //   }
  //   return datosCorrectos;
  // }
    //ingreso de datos
    datos=req.body;
    console.log(datos)
    connection.query("INSERT INTO clientes SET ?", {
        Cedula_Cliente: datos.Cedula_Cliente,
        Nombres_Cliente: datos.Nombres_Cliente,
        Direccion_Cliente: datos.Direccion_Cliente,
        Telefono_Cliente: datos.Telefono_Cliente,
        Correo_Cliente: datos.Correo_Cliente,
      });
    res.render("formulario", { valor:true,mensaje: "ingreso realizado exitosamente" });
});
app.listen(4000, (req, res) => {
    console.log("SERVER RUNNING IN http://localhost:4000");
  });