//Arranca la página

const express = require('express');
const morgan = require('morgan');
const { engine } = require('express-handlebars');
const path = require('path');
const exp = require('constants');
const flash = require('connect-flash');
const session = require('express-session');
const MYSQLStore = require('express-mysql-session');
const { database } = require('./keys');
const passport = require('passport');
const Handlebars = require("handlebars");


//Inicializaciones de servicios
const app = express();
require('./lib/passport');

//Settings
app.set('port', process.env.PORT || 4000);
app.set('views', path.join(__dirname, 'views'));
app.engine('.hbs', engine({
    defaultLayout: 'main',
    layoutsDir: path.join(app.get('views'), 'layouts'),
    partialsDir: path.join(app.get('views'), 'partials'),
    extname: '.hbs',
    helpers: require('./lib/handlebars')
}));
app.set('views engine', '.hbs');

//Middleware
app.use(session({
    secret: 'sesiones',
    resave: false,
    saveUninitialized: false,
    store: new MYSQLStore(database)

}))
app.use(flash());
app.use(morgan('dev'));
app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use(passport.initialize());
app.use(passport.session());

//Variables globales
app.use((req, res, next) => {
    app.locals.success = req.flash('success');
    app.locals.message = req.flash('message');
    app.locals.user = req.user;
    next();
});

//Routes
app.use(require('./routes'));
app.use(require('./routes/authentication'));
app.use('/links', require('./routes/links'));
app.use('/report', require('./routes/report'))


//Public
app.use(express.static(path.join(__dirname, 'public')));


//Starting server
app.listen(app.get('port'), () => {

    console.log('Server on port', app.get('port'));

});

//Auxiliares
Handlebars.registerHelper("ife", function(lvalue, rvalue, options) {
    if (lvalue === rvalue) {
        return options.fn(this);
    }
    return options.inverse(this);
});