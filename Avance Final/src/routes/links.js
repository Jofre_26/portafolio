const express = require('express');
const router = express.Router();

const db = require('../database');

const { estaLogueado } = require('../lib/auth');

router.get('/justificacion', estaLogueado, (req, res) => {
    res.render('justificaciones/justificacion.hbs')
});


router.post('/justificacion', estaLogueado, async(req, res) => {
    const { materia, docente, descripcion} = req.body;
    const newJustificacion = {
        materia,
        docente,
        descripcion,
        user_id:req.user.id
    };
    await db.query('INSERT INTO justificante set ?', [newJustificacion]);
    req.flash('success', 'Justificación guardada correctamente.');
    res.redirect('/links');
});

router.get('/', estaLogueado, async(req, res) => {
    const justificaciones = await db.query('SELECT * FROM justificante where user_id=?',[req.user.id]);
    res.render('justificaciones/list_justi.hbs', { justificaciones });
});

router.get('/eliminar/:id', estaLogueado, async(req, res) => {
    const { id } = req.params;
    await db.query('DELETE FROM justificante WHERE id = ?', [id]);
    req.flash('success', 'Justificación eliminada correctamente.');
    res.redirect('/links');
});

router.get('/editar_justi/:id', estaLogueado, async(req, res) => {
    const { id } = req.params;
    const justi = await db.query('SELECT * FROM justificante WHERE id = ?', [id]);
    res.render('justificaciones/editar_justi.hbs', { justi: justi[0] });
})

router.post('/editar_justi/:id', estaLogueado, async(req, res) => {
    const { id } = req.params;
    const { materia, docente, descripcion } = req.body;
    const newJustificacion = {
        materia,
        docente,
        descripcion
    };
    await db.query('UPDATE justificante set ? WHERE id = ?', [newJustificacion, id]);
    req.flash('success', 'Justificación actualizada correctamente.');
    res.redirect('/links');
})

module.exports = router;