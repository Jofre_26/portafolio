const express = require('express');
const { authenticate } = require('passport');
const router = express.Router();
const passport = require('passport');
const { estaLogueado, noEstaLogueado } = require('../lib/auth');

router.get('/registro', noEstaLogueado, (req, res) => {
    res.render('sistema/registro.hbs')
});

router.post('/registro', noEstaLogueado, passport.authenticate('local.registro', {
    successRedirect: '/perfil',
    failureRedirect: '/registro',
    failureFlash: true
}));

router.get('/login', noEstaLogueado, (req, res) => {
    res.render('sistema/login.hbs');
});

router.post('/login', noEstaLogueado, (req, res, next) => {
    passport.authenticate('local.login', {
        successRedirect: '/perfil',
        failureRedirect: '/login',
        failureFlash: true
    })(req, res, next);
});

router.get('/perfil', estaLogueado, (req, res) => {
    res.render('perfil.hbs');
});

router.get('/logout', (req, res) => {
    req.logOut();
    res.redirect('/login');
});

module.exports = router;