CREATE DATABASE database_secretaria;

USE database_secretaria;

CREATE TABLE users(
    id INT(11) NOT NULL,
    username VARCHAR(16) NOT NULL,
    password VARCHAR(60) NOT NULL,
    fullname VARCHAR(100) NOT NULL,
    direccion VARCHAR(50) NOT NULL,
    telefono INT(10) NOT NULL,
    rol VARCHAR(50) NOT NULL
);

ALTER TABLE users
    ADD PRIMARY KEY (id);

ALTER TABLE users
    MODIFY id INT(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT = 1;

DESCRIBE users;

CREATE TABLE justificante(
    id INT(11) NOT NULL,
    materia VARCHAR(50) NOT NULL,
    docente VARCHAR(50) NOT NULL,
    descripcion TEXT,
    user_id INT(11),
    created_at timestamp NOT NULL DEFAULT current_timestamp,
    CONSTRAINT fk_user FOREIGN KEY (user_id) REFERENCES users(id)
);

ALTER TABLE justificante
    ADD PRIMARY KEY (id);

ALTER TABLE justificante
    MODIFY id INT(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT = 1;

CREATE TABLE reporte(
    id INT(11) NOT NULL,
    estudiante VARCHAR(50) NOT NULL,
    materia VARCHAR(50) NOT NULL,
    paralelo INT(2) NOT NULL,
    docente_id INT(11),
    created_at timestamp NOT NULL DEFAULT current_timestamp,
    CONSTRAINT fk_docente FOREIGN KEY (docente_id) REFERENCES users(id)
);

ALTER TABLE reporte
    ADD PRIMARY KEY (id);

ALTER TABLE reporte
    MODIFY id INT(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT = 1;