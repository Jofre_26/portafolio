const expresiones = {
    usuario: /^[a-zA-Z0-9_-]{4,16}$/, // Letras, numeros, guion y guion_bajo
    password: /^[a-zA-Z0-9_-]{4,12}$/ // 4 a 12 carácteres.
}

function revisar(elemento) {
    if (elemento.value == '') {
        elemento.className = 'error';
    } else {
        elemento.className = 'input';
    }
}

function revisarUsuario(elemento) {
    if (elemento.value !== '') {
        var data = elemento.value;
        if (!expresiones.usuario.test(data)) {
            elemento.className = 'error';
        } else {
            elemento.className = 'input';
        }
    }
}

function revisarPassword(elemento) {
    if (elemento.value !== '') {
        var data = elemento.value;
        if (!expresiones.password.test(data)) {
            elemento.className = 'error';
        } else {
            elemento.className = 'input';
        }
    }
}

function validar(elemento) {
    var datosCorrectos = true;
    var error1 = "";
    var error2 = "";


    if (document.getElementById("username").value == "" || !expresiones.usuario.test(document.getElementById("username").value)) {
        datosCorrectos = false;
        error1 = "\nEl usuario debe tener de 4 a 16 carácteres"
    }


    if (document.getElementById("password").value == "" || !expresiones.usuario.test(document.getElementById("password").value)) {
        datosCorrectos = false;
        error2 = "\nLa contraseña debe tener de 4 a 12 carácteres"
    }

    if (!datosCorrectos) {
        alert('Hay errores en el formulario' + error1 + error2);
    }

    return datosCorrectos;
}