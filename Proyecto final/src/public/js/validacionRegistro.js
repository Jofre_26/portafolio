const expresiones = {
    nombre: /^[a-zA-ZÀ-ÿ\s]{1,50}$/, // Letras y espacios, pueden llevar acentos.
    direccion: /^[a-zA-ZÀ-ÿ\s]{1,50}$/, // Letras y espacios, pueden llevar acentos.
    usuario: /^[a-zA-Z0-9\_\-]{4,16}$/, // Letras, numeros, guion y guion_bajo
    password: /^[a-zA-Z0-9\_\-]{4,12}$/, // 4 a 12 carácteres.
    telefono: /^[+]*[(]?[0-9]{1,4}[)]?[0-9-\s\.]{1,6}$/ // 1 a 10 digitos.
}

function revisar(elemento) {
    if (elemento.value == '') {
        elemento.className = 'error';
    } else {
        elemento.className = 'input';
    }
}

function revisarNombre(elemento) {
    if (elemento.value !== '') {
        var data = elemento.value;
        if (!expresiones.nombre.test(data)) {
            elemento.className = 'error';
        } else {
            elemento.className = 'input';
        }
    }
}

function revisarUsuario(elemento) {
    if (elemento.value !== '') {
        var data = elemento.value;
        if (!expresiones.usuario.test(data)) {
            elemento.className = 'error';
        } else {
            elemento.className = 'input';
        }
    }
}

function revisarPassword(elemento) {
    if (elemento.value !== '') {
        var data = elemento.value;
        if (!expresiones.password.test(data)) {
            elemento.className = 'error';
        } else {
            elemento.className = 'input';
        }
    }
}

function revisarDireccion(elemento) {
    if (elemento.value !== '') {
        var data = elemento.value;
        if (!expresiones.direccion.test(data)) {
            elemento.className = 'error';
        } else {
            elemento.className = 'input';
        }
    }
}

function revisarTelefono(elemento) {
    if (elemento.value !== '') {
        var data = elemento.value;
        if (!expresiones.telefono.test(data)) {
            elemento.className = 'error';
        } else {
            elemento.className = 'input';
        }
    }
}

function validar() {
    var datosCorrectos = true;
    var error1 = "";
    var error2 = "";
    var error3 = "";
    var error4 = "";
    var error5 = "";

    if (document.getElementById("fullname").value == "" || !expresiones.nombre.test(document.getElementById("fullname").value)) {
        datosCorrectos = false;
        error1 = "\nEl nombre debe ser de 1 a 50 carácteres"
    }

    if (document.getElementById("username").value == "" || !expresiones.usuario.test(document.getElementById("username").value)) {
        datosCorrectos = false;
        error2 = "\nEl usuario debe tener de 4 a 16 carácteres"
    }

    if (document.getElementById("password").value == "" || !expresiones.password.test(document.getElementById("password").value)) {
        datosCorrectos = false;
        error3 = "\nLa contraseña debe tener de 4 a 12 carácteres"
    }

    if (document.getElementById("direccion").value == "" || !expresiones.direccion.test(document.getElementById("direccion").value)) {
        datosCorrectos = false;
        error4 = "\nLa direccion debe ser de 1 a 50 carácteres"
    }

    if (document.getElementById("telefono").value == "" || !expresiones.telefono.test(document.getElementById("telefono").value)) {
        datosCorrectos = false;
        error5 = "\nEl teléfono solamente son números, de 1 a 10 dígitos"
    }

    if (!datosCorrectos) {
        alert('Hay errores en el formulario' + error1 + error2 + error3 + error4 + error5);
    }

    return datosCorrectos;
}