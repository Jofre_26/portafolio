let map;

function initMap() {
    map = new google.maps.Map(document.getElementById("map"), {
        center: {
            lat: -0.9530880471804638,
            lng: -80.7457407821637
        },
        zoom: 15,
    });
}