const express = require('express');
const router = express.Router();

const db = require('../database');

const { estaLogueado } = require('../lib/auth');

router.get('/reporte', estaLogueado, (req, res) => {
    res.render('reportes/reporte.hbs')
});

router.post('/reporte', estaLogueado, async(req, res) => {
    const { estudiante, materia, paralelo, motivo } = req.body;
    const newReporte = {
        estudiante,
        materia,
        paralelo,
        motivo,
        docente_id:req.user.id
    };
    await db.query('INSERT INTO reporte set ?', [newReporte]);
    res.redirect('/report');
});

router.get('/', estaLogueado, async(req, res) => {
    const reportes = await db.query('SELECT * FROM reporte where docente_id=?',[req.user.id]);
    res.render('reportes/list_reporte.hbs', { reportes });
});

router.get('/eliminar/:id', estaLogueado, async(req, res) => {
    const { id } = req.params;
    await db.query('DELETE FROM reporte WHERE id = ?', [id]);
    res.redirect('/report');
});

router.get('/editar_reporte/:id', estaLogueado, async(req, res) => {
    const { id } = req.params;
    const rep = await db.query('SELECT * FROM reporte WHERE id = ?', [id]);
    res.render('reportes/editar_reporte.hbs', { rep: rep[0] });
});

router.post('/editar_reporte/:id', estaLogueado, async(req, res) => {
    const { id } = req.params;
    const { estudiante, materia, paralelo, motivo } = req.body;
    const newReporte = {
        estudiante,
        materia,
        paralelo,
        motivo
    };
    await db.query('UPDATE reporte set ? WHERE id = ?', [newReporte, id]);
    res.redirect('/report');
});

module.exports = router;