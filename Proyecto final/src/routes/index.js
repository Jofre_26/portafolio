const express = require('express');
const router = express.Router();

router.get('/', (req, res) => {
    res.render('index.hbs');
});

router.get('/nosotros', (req, res) => {
    res.render('nosotros.hbs');
});

router.get('/contacto', (req, res) => {
    res.render('contacto.hbs');
});

module.exports = router;