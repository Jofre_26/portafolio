//Conexion BD

const mysql = require('mysql');
const { promisify } = require('util');

const { database } = require('./keys');

const db = mysql.createPool(database);

db.getConnection((err, connection) => {
    if (err) {
        if (err.code === 'PROTOCOL_CONNECTION_LOST') {
            console.error('LA CONEXION CON LA BASE DE DATOS FUE CERRADA');
        }
        if (err.code === 'ER_CON_COUNT_ERROR') {
            console.error('LA BASE DE DATOS TIENE MUCHAS CONEXIONES');
        }
        if (err.code === 'ECONNREFUSED') {
            console.error('LA CONEXION CON LA BASE DE DATOS FUE RECHAZADA')
        }
    }

    if (connection) connection.release();
    console.log('BASE DE DATOS CONECTADA CORRECTAMENTE');

    return;

});

//SE CONVIERTE EN PROMESAS LO QUE ANTES ERAN CALLBACKS
db.query = promisify(db.query);

module.exports = db;